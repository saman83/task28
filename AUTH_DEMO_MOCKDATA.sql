USE authdemo;

INSERT INTO user (username, password) VALUES ('admin', '$2y$10$p7OxPWLbzSZ0rQv1sHNaAeHWzA4U49KC8degMYtJy5Z8g7CxglNzu'); -- sup3rs3cr3t
INSERT INTO user (username, password) VALUE ('test', '$2y$10$wyhk57n0gAwFWpR2wd0QHeoBZBt3Vkhz/Ftc5Zu22lG75R4FT.Ude'); -- nots3cr3t

SELECT * FROM user;